CC			= gcc
CXX			= g++
RM			= rm -f

LIB			= libstack.so
TEST		= libstack-test
CFLAGS		= -O2 -shared -fPIC -Wall -Wextra -Werror -I./include
CXXFLAGS	= -O2 -shared -fPIC -Wall -Wextra -Werror -I./include
LDFLAGS		= -O2 -shared -fPIC -rdynamic

SRCS		= src/stub.c
OBJS		= $(SRCS:.c=.o)

$(LIB):		$(OBJS)
			$(CC) $(OBJS) -o $(LIB) $(LDFLAGS)

all: 		$(LIB)

clean:
			$(RM) $(OBJS)

fclean:		clean
			$(RM) $(LIB)

re:			fclean all

install:	all
			$(CP) $(LIB) /usr/local/lib

test:		re $(TEST)
			./$(TEST)

.PHONY:		all clean fclean re install test