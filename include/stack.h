#ifndef STACK_H
#define STACK_H

#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>

typedef struct s_stack {
    void *  top; // top of the stack
    void *  bottom; // bottom of the stack
    void *  sp; // current stack pointer value
    void *  bp; // current base pointer value
} stack_t;

typedef ssize_t (*sfunc)();
typedef int (*mfunc)();

void * salloc(size_t size);
void * srealloc(void * ptr, size_t size);
void * smove(void * dest, const void * ptr);
void * snmove(void * dest, const void * ptr, size_t n);
void * scat(void * dest, const void * src);
void * sncat(void * dest, const void * src, size_t n);
void * sdup(const void * ptr);
void * sndup(const void * ptr, size_t n);
ssize_t slen(const void * ptr);
const stack_t * sinfo(const void * ptr);
void sfree(void * ptr);
ssize_t sdefer(void * ptr, const sfunc f);
int smain(void * ptr, const mfunc f); // return only on error
int sfreset(void * addr); // remove all stack frames and reset pointers to top of stack
void remove_main_stack(void);

#endif /* STACK_H */
