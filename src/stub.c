#include <stdlib.h>
#include "stack.h"

void * salloc(size_t size) {
    (void)size;
    return NULL;
}

void * srealloc(void * ptr, size_t size) {
    (void)ptr;
    (void)size;
    return NULL;
}

void * smove(void * dest, const void * ptr) {
    (void)dest;
    (void)ptr;
    return NULL;
}

void * snmove(void * dest, const void * ptr, size_t n) {
    (void)dest;
    (void)ptr;
    (void)n;
    return NULL;
}

void * scat(void * dest, const void * src) {
    (void)dest;
    (void)src;
    return NULL;
}

void * sncat(void * dest, const void * src, size_t n) {
    (void)dest;
    (void)src;
    (void)n;
    return NULL;
}

void * sdup(const void * ptr) {
    (void)ptr;
    return NULL;
}

void * sndup(const void * ptr, size_t n) {
    (void)ptr;
    (void)n;
    return NULL;
}

ssize_t slen(const void * ptr) {
    (void)ptr;
    return 0;
}

const stack_t * sinfo(const void * ptr) {
    (void)ptr;
    return NULL;
}

void sfree(void * ptr) {
    (void)ptr;
}

ssize_t sdefer(void * ptr, const sfunc f) {
    (void)ptr;
    (void)f;
    return 0;
}

int smain(void * ptr, const mfunc f) {
    (void)ptr;
    (void)f;
    return 0;
}

int sfreset(void * addr) {
    (void)addr;
    return 0;
}

void remove_main_stack(void) {
}